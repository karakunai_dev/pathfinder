#!/bin/sh

# Launch
xdotool type 'dart app.dart'
xdotool key "Return"

# Node A
xdotool type 'A'
xdotool key "Return"
xdotool type 'B,C'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node B
xdotool type 'B'
xdotool key "Return"
xdotool type 'A,K,E,C'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node K
xdotool type 'K'
xdotool key "Return"
xdotool type 'B'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node E
xdotool type 'E'
xdotool key "Return"
xdotool type 'B,C,J,F'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node J
xdotool type 'J'
xdotool key "Return"
xdotool type 'E'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node C
xdotool type 'C'
xdotool key "Return"
xdotool type 'A,B,E,D'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node D
xdotool type 'D'
xdotool key "Return"
xdotool type 'C,F,L'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node E
xdotool type 'E'
xdotool key "Return"
xdotool type 'B,C,D,F'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '6'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node L
xdotool type 'L'
xdotool key "Return"
xdotool type 'D'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node F
xdotool type 'F'
xdotool key "Return"
xdotool type 'D,E,G'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node G
xdotool type 'G'
xdotool key "Return"
xdotool type 'F,M,H'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node M
xdotool type 'M'
xdotool key "Return"
xdotool type 'G'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node H
xdotool type 'H'
xdotool key "Return"
xdotool type 'G,I'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node I
xdotool type 'I'
xdotool key "Return"
xdotool type 'H'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'no'
xdotool key "Return"