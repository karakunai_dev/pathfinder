/*
  Copyright 2020 Ronan Harris
  Licensed under MIT
  twitter.com/Ronanharris4
*/

// import 'dart:convert';
import 'dart:collection';
import 'dart:io';
import 'package:test/test.dart';
// import 'package:http/http.dart' as http;
import 'path.dart';
import 'node.dart';
import 'route.dart';

// Grab a lists of nodes and find the matching node based on the input string name of a node
int getIndex(dynamic aList, String aString) {

  String referencedValue;

  for (var i = 0; i < aList.length; i++) {

    // Check the type of the List and give the value to referencedValue
    if (aList is List<Node>) {
      referencedValue = aList[i].nodeName;
    }
    
    if (aList is List<Path>) {
      referencedValue = aList[i].connectedNode;
    }

    // If it is not any of those 2 Lists defined above, go straight
    // and give the value at [i] to referencedValue
    if (referencedValue == null) {
      referencedValue = aList[i];
    }

    if (referencedValue == aString) {
      return i;
    }
  }  
  
  return -1;
}

bool countString(dynamic aList, String aString) {
  int occurences = 0;

  for (var item in aList) {
    if (item == aString) {
      occurences++;
    }
  }

  if (occurences == aList.length) {
    return true;
  }

  return false;
}

void main() async {

  // A planned changes to read the inputs by JSON
  // var newResponse = await http.get('https://jsonplaceholder.typicode.com/todos');
  // var zz = JsonCodec().decode(newResponse.body);
  // print("Response Status = ${newResponse.statusCode}");
  // print("Response Body = ${zz[2]['title']}");

  print("Welcome to pathFinder by Ronan Harris.\nVersion 0.1.0");

  List <Node> nodeCollection = new List();

  // Node initialization Start
  while(true) {

    Node newNode = new Node();

    stdout.writeln("Enter Node Name : ");
    newNode.nodeName = stdin.readLineSync();

    stdout.writeln("Mention Below Other Nodes That are Directly Connected to Node '${newNode.nodeName}', Separated by Commas.");

    for (var newPath in stdin.readLineSync().split(',').toList()) {
      stdout.writeln("How Long is The Distance From '${newNode.nodeName}' to '${newPath}' ?");
      newNode.directPaths.add(new Path(connectedNode: newPath.toString(), distance: int.tryParse(stdin.readLineSync())));
    }
    
    nodeCollection.add(newNode);

    stdout.writeln("Add Another Node ? (yes/no)");
    var continueLoop = stdin.readLineSync().toLowerCase();

    if (!(continueLoop == 'y' || continueLoop == 'yes')) {
      break;
    }
  }
  // Node initialization End

  // Initializing startingNode and destinationNode Start
  print("Here is the lists of node stored : ");

  for (var i = 0; i < nodeCollection.length; i++) {
    stdout.write("[$i] ${nodeCollection.elementAt(i).nodeName}  ");
  }

  stdout.write("\nWhat's the starting node : ");
  int startingNode = int.parse(stdin.readLineSync());

  stdout.write("What's the destination node : ");
  int destinationNode = int.parse(stdin.readLineSync());
  // Initializing startingNode and destinationNode End

  // Route Analysis Start
  ListQueue <String> temporaryStack1D = new ListQueue();
  List <String> visitedNode = new List();

  temporaryStack1D.add(nodeCollection.elementAt(startingNode).nodeName);

  var internalTimer = new Stopwatch();
  internalTimer.start();

  while(!countString(temporaryStack1D, nodeCollection.elementAt(destinationNode).nodeName)) {
    List <String> aList = new List();

    String currentTop = temporaryStack1D.first;
    int topIndex = 0;
    
    while (currentTop == nodeCollection.elementAt(destinationNode).nodeName) {
      topIndex++;
      currentTop = temporaryStack1D.elementAt(topIndex);
    }
    
    for (var item in nodeCollection.elementAt(getIndex(nodeCollection, currentTop)).directPaths) {
      if (!visitedNode.contains(item.connectedNode)) {
        aList.add(item.connectedNode);
      }
    }

    for (var i = 0; i < aList.length; i++) {
      nodeCollection.elementAt(getIndex(nodeCollection, aList[i])).knownNode.add(currentTop);
    }

    temporaryStack1D.addAll(aList);
    
    if (!visitedNode.contains(currentTop)) {
      temporaryStack1D.remove(currentTop);
      
      if (!temporaryStack1D.contains(currentTop)) {
        visitedNode.add(currentTop);
      } else {
        temporaryStack1D.remove(currentTop);
        temporaryStack1D.addFirst(currentTop);
      }
    }
  }
    // Route Analysis End

  // Route Creation Start
  // Below are the algorithm  to find every possible route available by using the data from route analysis process stored in knownNode
  ListQueue <Route> possibleRoutes = new ListQueue();
  
  for (var i = nodeCollection.elementAt(destinationNode).knownNode.length; i > 0; i--) {
    ListQueue <String> newRoute = new ListQueue();

    newRoute.add(nodeCollection.elementAt(destinationNode).nodeName);
    newRoute.addFirst(nodeCollection.elementAt(destinationNode).knownNode.removeFirst());

    while (newRoute.first != nodeCollection.elementAt(startingNode).nodeName) {
      if (nodeCollection.elementAt(getIndex(nodeCollection, newRoute.first)).knownNode.length > 1) {
        newRoute.addFirst(nodeCollection.elementAt(getIndex(nodeCollection, newRoute.first)).knownNode.removeFirst());
      } else {
        newRoute.addFirst(nodeCollection.elementAt(getIndex(nodeCollection, newRoute.first)).knownNode.first);
      }
    }

    possibleRoutes.add(new Route(knownRoute: newRoute));
  }
  // Route Creation End

  // Route Distance Calculation Start
  for (var i = 0; i < possibleRoutes.length; i++) {
    
    ListQueue <String> temporaryStack = new ListQueue();
    temporaryStack.addAll(possibleRoutes.elementAt(i).knownRoute);

    while (temporaryStack.length > 1) {
      String currentNode = temporaryStack.removeFirst();
      possibleRoutes.elementAt(i).totalDistance += nodeCollection.elementAt(getIndex(nodeCollection, currentNode)).directPaths.elementAt(getIndex(nodeCollection.elementAt(getIndex(nodeCollection, currentNode)).directPaths, temporaryStack.first)).distance;
    }
  }
  // Route Distance Calculation End

  // Print Result (Temporary Code, Will Change)
  for (var i = 0; i < possibleRoutes.length; i++) {
    print("ROUTE : ${possibleRoutes.elementAt(i).knownRoute} | DISTANCE : ${possibleRoutes.elementAt(i).totalDistance}");
  }

  internalTimer.stop();
  print("Time Elapsed : ${internalTimer.elapsed}");
}