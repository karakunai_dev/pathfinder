/*
  Copyright 2020 Ronan Harris
  Licensed under MIT
  twitter.com/Ronanharris4
*/

import 'dart:collection';

class Route {
  int totalDistance;
  ListQueue <String> knownRoute = new ListQueue();

  Route({this.totalDistance = 0, this.knownRoute = null});
}