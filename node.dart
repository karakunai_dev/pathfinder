/*
  Copyright 2020 Ronan Harris
  Licensed under MIT
  twitter.com/Ronanharris4
*/

import 'dart:collection';
import 'path.dart';

// Class to represent a node that holds the properties of which paths its connected to
class Node {
  String nodeName;
  List <Path> directPaths = new List();
  ListQueue <String> knownNode = new ListQueue();

  Node({this.nodeName = ''});
}